<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Hierarchy extends Controller
{
    public function show() 
    {
    	$result = DB::table('employee')->get()->toArray();
    	$tree = [];
    	foreach ($result as $k) {
    		$tree[$k->boss_id][$k->id] = (array) $k;
    	}
    	return view('index', [
    		'tree' => self::render($tree, 0),
    	]);
    }

    public static function render($employee, $boss_id) 
    {
    	if (is_array($employee) and isset($employee[$boss_id])) {
	        $hierarchy = '<ul>';
            foreach ($employee[$boss_id] as $employe) {
                $hierarchy .= '<li>'.'<p><span>id:</span> '.$employe['id']
                	.'</p><p><span>name:</span> '.$employe['full_name']
                	.'</p><p><span>salary:</span> '.$employe['salary']
                	.'</p><p><span>start_date:</span> '.$employe['start_date']
                	.'</p><p><span>boss_id:</span> '.$employe['boss_id'];
                $hierarchy .=  self::render($employee, $employe['id']);
                $hierarchy .= '</li>';
            }
	        $hierarchy .= '</ul>';
	    } else {
	    	return null;
	    } 

	    return $hierarchy;
    }
}
