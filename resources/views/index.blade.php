<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            p {
                margin: 0;
                font-size: 20px;
            }

            p span {
                font-weight: bold;
            }

            ul {
                list-style-type: none;
                margin: 30px 0px;
            }
        </style>
    </head>
    <body>
        {!! $tree !!}
    </body>
</html>
