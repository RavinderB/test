<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee')->insert([
        	['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 0, 'position' => 'генерал 1', 'salary' => 9000],
        	['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 0, 'position' => 'генерал 2', 'salary' => 9000],
        	['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 2, 'position' => 'полковник 1', 'salary' => 8000],
			['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 1, 'position' => 'полковник 2', 'salary' => 8000],
			['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 3, 'position' => 'подполковник', 'salary' => 7000],
			['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 4, 'position' => 'майор', 'salary' => 6000],
			['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 5, 'position' => 'капитан', 'salary' => 5000],
			['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 6, 'position' => 'старший лейтенант', 'salary' => 4000],
			['full_name' => 'employee'.rand(1, 9000), 'boss_id' => 7, 'position' => 'лейтенант', 'salary' => 3000]
        ]);
    }
}
